/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

public class ScreenHandler {
    private static final String TAG = "ScreenHandler";

    private static final long TIME_SCREEN_ON = 5000;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private DevicePolicyManager policyManager;

    private Settings settings;

    private long lastTimeScreenOnOrOff;

    private final Context context;

    private static volatile ScreenHandler instance;

    private Thread turnOffScreenThread;
    private static final long MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_OFF = 1000;

    public static ScreenHandler getInstance(Context context) {
        if (instance == null ) {
            synchronized (ScreenHandler.class) {
                if (instance == null) {
                    instance = new ScreenHandler(context);
                }
            }
        }

        return instance;
    }

    private ScreenHandler(Context context) {
        this.context = context;
        this.powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        this.wakeLock = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "WakeUpWakeLock");
        this.policyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        this.settings = Settings.getInstance(context);
    }

    public boolean isScreenOn() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) { // isScreenOn method is deprecated from API Level 20
            return powerManager.isInteractive();
        } else {
            return powerManager.isScreenOn();
        }
    }

    private Thread turnOffScreenThread(final long delay) {
        return new Thread() {
            @Override
            public void run() {
                if (isScreenOn()) {
                    Logger.d(context, TAG, "Creating a thread to turn off display if still covered in " + delay/1000 + " seconds");
                    try {
                        Thread.sleep(delay);
                        doTurnOffScreen();
                    } catch (InterruptedException e) {
                        Logger.d(context, TAG, "Interrupted thread: Uncovered sensor too fast. Turning off screen cancelled.");
                    }
                }
            }
        };
    }

    private void doTurnOffScreen() {
        lastTimeScreenOnOrOff = System.currentTimeMillis();
        Logger.i(context, TAG, "Switched from 'far' to 'near'.");
        if (settings.isLockScreenWithPowerButton()) {
            Logger.i(context, TAG, "Turning screen off simulating power button press.");
            Root.pressPowerButton();
        } else {
            Logger.i(context, TAG, "Turning screen off.");
            policyManager.lockNow();
        }
    }

    public void turnOffScreen() {
        turnOffScreenThread = turnOffScreenThread(MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_OFF);
        turnOffScreenThread.start();
    }

    public void cancelTurnOff() {
        if (turnOffScreenThread != null) {
            turnOffScreenThread.interrupt();
        }
    }

    public long getLastTimeScreenOnOrOff() {
        return lastTimeScreenOnOrOff;
    }

    public void turnOnScreen() {
        if (!isScreenOn()) {
            lastTimeScreenOnOrOff = System.currentTimeMillis();
            Logger.i(context, TAG, "Switched from 'near' to 'far'. Turning screen on");
            wakeLock.acquire(TIME_SCREEN_ON);
        }
    }
}