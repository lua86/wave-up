/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.jarsilio.android.waveup.Logger;
import com.jarsilio.android.waveup.ProximitySensorManager;
import com.jarsilio.android.waveup.Settings;

public class ScreenReceiver extends BroadcastReceiver {
    private static final String TAG = "ScreenReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Settings settings = Settings.getInstance(context);
        if (settings.isServiceEnabled()) {
            ProximitySensorManager proximityManager = ProximitySensorManager.getInstance(context);
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                Logger.d(context, TAG, "Screen off");
                if (!settings.isPocketMode() && !settings.isWaveMode()) {
                    Logger.d(context, TAG, "Stopping proximity sensor because screen is off and no wave up options are enabled");
                    proximityManager.stop();
                } else {
                    proximityManager.start();
                }
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                Logger.d(context, TAG, "Screen on");
                if (!settings.isLockScreen()) {
                    Logger.d(context, TAG, "Stopping proximity sensor because screen is on and lock is not enabled");
                    proximityManager.stop();
                } else {
                    proximityManager.start();
                }
            }
        }
    }
}