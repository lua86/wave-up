/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.jarsilio.android.waveup.receivers.LockScreenAdminReceiver;

public class MainActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "MainActivity";
    private static final int ADD_DEVICE_ADMIN_REQUEST_CODE = 1;

    private static final String[] WRITE_EXTERNAL_STORAGE_PERMISSION = {"android.permission.WRITE_EXTERNAL_STORAGE"};
    private static final String[] READ_PHONE_STATE_PERMISSION = {"android.permission.READ_PHONE_STATE"};

    private static final int WRITE_EXTERNAL_STORAAGE_PERMISSION = 200;
    private static final int READ_PHONE_STATE_PERMISSION_REQUEST_CODE = 300;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d(getApplicationContext(), TAG, "onCreate");
        super.onCreate(savedInstanceState);
        createLayout();
        if (!getSettings().isLockScreenAdmin()) {
            requestLockScreenAdminRights();
        }
        updateEnabledPreferences();
        requestReadPhoneStatePermission();
        startService();
        registerPreferencesListener();
    }

    private Settings getSettings() {
        return Settings.getInstance(getApplicationContext());
    }

    private void createLayout() {
        LinearLayout linearLayout = (LinearLayout) findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar toolbar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.settings_toolbar, linearLayout, false);
        linearLayout.addView(toolbar, 0); // insert at top
        addPreferencesFromResource(R.xml.settings);
        Button uninstallButton = new Button(getApplicationContext());
        uninstallButton.setText(R.string.uninstall_button);
        uninstallButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                uninstallApp();
            }
        });

        ListView listView = getListView();
        listView.addFooterView(uninstallButton);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case Settings.ENABLED:
                updateEnabledPreferences();
                startService();
                break;
            case Settings.LOCK_SCREEN:
                updateEnabledPreferences();
                if (getSettings().isLockScreen()) {
                    if (getSettings().isLockScreenAdmin()) {
                        ProximitySensorManager.getInstance(getApplicationContext()).start();
                    } else {
                        requestLockScreenAdminRights();
                    }
                }
                break;
            case Settings.WRITE_TO_LOGFILE:
                if (getSettings().isWriteToLogfile() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    // If user wants to write to log file and uses Android Marshmallow or newer, request permission to write to file.
                    requestPermissions(WRITE_EXTERNAL_STORAGE_PERMISSION, WRITE_EXTERNAL_STORAAGE_PERMISSION);
                }
                break;
            case Settings.LOCK_SCREEN_WITH_POWER_BUTTON:
                if (getSettings().isLockScreenWithPowerButton()) {
                    Root.requestSuPermission();
                }
                break;
        }
    }

    private void startService() {
        if (getSettings().isServiceEnabled()) {
            Logger.i(getApplicationContext(), TAG, "Starting WaveUpService");
            startService(new Intent(this, WaveUpService.class));
            Toast.makeText(this, R.string.wave_up_service_started, Toast.LENGTH_SHORT).show();
        } else {
            Logger.i(getApplicationContext(), TAG, "Stopping WaveUpService");
            stopService(new Intent(this, WaveUpService.class));
            Toast.makeText(this, R.string.wave_up_service_stopped, Toast.LENGTH_SHORT).show();
        }
    }

    private void requestReadPhoneStatePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(READ_PHONE_STATE_PERMISSION, READ_PHONE_STATE_PERMISSION_REQUEST_CODE);
        }
    }

    private void requestLockScreenAdminRights() {
        ComponentName lockScreenAdminComponentName = new ComponentName(getApplicationContext(), LockScreenAdminReceiver.class);
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, lockScreenAdminComponentName);
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, R.string.lock_admin_rights_explanation);
        startActivityForResult(intent, ADD_DEVICE_ADMIN_REQUEST_CODE);
    }

    private void updateEnabledPreferences() {
        for (String key : getSettings().getPropertyKeys()) {
            if (!key.equals(Settings.ENABLED)) {
                boolean flag = getSettings().isServiceEnabled();
                if (key.equals(Settings.LOCK_SCREEN_WHEN_LANDSCAPE) || key.equals(Settings.LOCK_SCREEN_WITH_POWER_BUTTON)) {
                    flag = flag & getSettings().isLockScreen();
                }
                findPreference(key).setEnabled(flag);
            }
        }
    }

    private void uninstallApp() {
        Logger.i(getApplicationContext(), TAG, "Removing lock screen admin rights");
        ComponentName devAdminReceiver = new ComponentName(getApplicationContext(), LockScreenAdminReceiver.class);
        DevicePolicyManager dpm = (DevicePolicyManager) getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
        dpm.removeActiveAdmin(devAdminReceiver);

        getSettings().setLockScreen(false);
        refreshPreferencesScreen();

        Logger.i(getApplicationContext(), TAG, "Uninstalling app");
        Uri packageURI = Uri.parse("package:" + "com.jarsilio.android.waveup");
        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
        startActivity(uninstallIntent);
    }

    private void refreshPreferencesScreen() {
        /* Dirty hack to refresh preferences screen when I change a value from the code */
        getPreferenceScreen().removeAll();
        addPreferencesFromResource(R.xml.settings);
        updateEnabledPreferences();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_DEVICE_ADMIN_REQUEST_CODE) {
            if (!getSettings().isLockScreenAdmin()) {
                // If the user does not activate lock admin switch off lock screen option
                getSettings().setLockScreen(false);
            }
            refreshPreferencesScreen();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshPreferencesScreen();
        registerPreferencesListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterPreferencesListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterPreferencesListener();
    }

    private void registerPreferencesListener() {
        getSettings().getPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    private void unregisterPreferencesListener() {
        getSettings().getPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
