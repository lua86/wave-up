/*
 * Copyright (c) 2016 Juan Garcia
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.jarsilio.android.waveup.receivers.CallStateReceiver;

public class ProximitySensorManager implements SensorEventListener {
    private static final String TAG = "ProximitySensorManager";

    private final SensorManager sensorManager;
    private final Sensor proximitySensor;

    private enum Distance { NEAR, FAR };

    private static final long POCKET_THRESHOLD = 2000; // Time from which it is considered that the device is in a pocket
    private static final long MIN_TIME_BETWEEN_SCREEN_ON_AND_OFF = 1500;

    private static final long MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_ON = 0;

    private Distance lastDistance = Distance.FAR;
    private long lastTime = 0;

    private final Context context;

    private static volatile ProximitySensorManager instance;
    private ScreenHandler screenHandler;

    private boolean listening = false;

    public static ProximitySensorManager getInstance(Context context) {
        if (instance == null ) {
            synchronized (ProximitySensorManager.class) {
                if (instance == null) {
                    instance = new ProximitySensorManager(context);
                }
            }
        }

        return instance;
    }

    private ProximitySensorManager(Context context) {
        this.context = context;
        this.screenHandler = ScreenHandler.getInstance(context);
        this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        this.proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        start();
    }

    public final void start () {
        if (Settings.getInstance(context).isServiceEnabled() && !listening) {
            Logger.v(context, TAG, "start(): WaveUp is enabled. Starting sensor listener.");
            initProximitySensor();
            listening = true;
        } else {
            Logger.v(context, TAG, "start(): WaveUp is NOT enabled or sensor is already listening. I will not start it.");
        }
    }

    public final void stop () {
        if (listening) {
            Logger.v(context, TAG, "stop(): Unregistering Listener");
            sensorManager.unregisterListener(this);
            listening = false;
        }
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        // If the sensor changes, there is possibly a thread waiting to turn off the screen. It needs to be interrupted.
        screenHandler.cancelTurnOff();
        if (CallStateReceiver.isOngoingCall()) {
            return;
        }

        Distance currentDistance = event.values[0] >= event.sensor.getMaximumRange() ? Distance.FAR : Distance.NEAR;
        long currentTime = System.currentTimeMillis();

        Logger.v(context, TAG,
                String.format("Proximity sensor changed: %s (current sensor value: %f - max. sensor value: %f)",
                        currentDistance, event.values[0], event.sensor.getMaximumRange()));

        boolean waveModeEnabled = Settings.getInstance(context).isWaveMode();
        boolean pocketModeEnabled = Settings.getInstance(context).isPocketMode();

        long timeSinceLastScreenOnOrOff = currentTime - screenHandler.getLastTimeScreenOnOrOff();
        long timeBetweenMeasures = currentTime - lastTime; // How long ago the sensor changed for the last time
        if (timeSinceLastScreenOnOrOff > MIN_TIME_BETWEEN_SCREEN_ON_AND_OFF) { // Don't do anything if it turned on or off 2 seconds ago
            if (lastDistance == Distance.NEAR && currentDistance == Distance.FAR
                    && timeBetweenMeasures > MIN_TIME_SENSOR_COVERED_TO_TURN_SCREEN_ON) { // Turn on?
                if ((pocketModeEnabled && timeBetweenMeasures > POCKET_THRESHOLD) ||
                        (waveModeEnabled && timeBetweenMeasures < POCKET_THRESHOLD)) {
                    screenHandler.turnOnScreen();
                }
            } else if (lastDistance == Distance.FAR && currentDistance == Distance.NEAR) { // Turn off?
                boolean lockScreenEnabled = Settings.getInstance(context).isLockScreen();
                boolean isLockScreenAdmin = Settings.getInstance(context).isLockScreenAdmin();
                boolean lockLandscapeEnabled = Settings.getInstance(context).isLockScreenWhenLandscape();
                boolean portrait = (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
                if (lockScreenEnabled && isLockScreenAdmin) {
                    if (lockLandscapeEnabled || portrait) {
                        screenHandler.turnOffScreen();
                    }
                }
            }
        } else {
            Logger.d(context, TAG, "Time since last screen on/off: " + timeSinceLastScreenOnOrOff + ". Not doing anything");
        }
        lastDistance = currentDistance;
        lastTime = currentTime;
    }


    @Override
    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    private void initProximitySensor() {
        Logger.d(context, TAG, "Initializing proximity sensor");
        sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
}