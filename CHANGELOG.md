# Changelog
All notable changes to this project will be documented in this file. Format inspired by http://keepachangelog.com/ and this example https://github.com/olivierlacan/keep-a-changelog/blob/master/CHANGELOG.md

## [0.97] - 2016-05-11
### Added
 - Suspend WaveUp while on a phone call (needs READ_PHONE_STATE permission)

## [0.96-2] - 2016-05-05
### Fixed
- Compatibility issues: improvement in near/far measurement. Some phones report strange values and this should fix it.

## [0.96-1] - 2016-05-04
### Fixed
- Fix crash at startup on some phones (upgraded appcompat)

## [0.96] - 2016-05-04
### Added
- Japanese translation. Thank you Tsuyoshi!
- Small improvement in lock settings. Thank you for this too Tsuyoshi! :)

## [0.95] - 2016-04-30
### Added
- Switch off screen simulating power button

### Changed
- Performance improvements

### Known issues
- Lock in landscape option isn't really working

## [0.94] - 2016-04-27
### Fixed
- App crash while starting for the first time after a boot (if disabled)

## [0.93] - 2016-04-22
### Added
- Lock in landscape mode option

## [0.92] - 2016-04-18
### Added
- Logging options (included log to file)

## [0.91] - 2016-04-10
### Added
- First version of WaveUp
