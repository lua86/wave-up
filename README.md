WaveUp
======

WaveUp is an Android app that *wakes up* your phone - switches the screen on - when you *wave* over the proximity sensor.

I have developed this app because I wanted to avoid pressing the power button just to take a look at the watch - which I happen to do a lot on my phone. There are already other apps that do exactly this - and even more. I was inspired by Gravity Screen On/Off, which is a **great** app. However, I am a huge fan of open source software and try to install free software (free as in freedom, not only free as in free beer) on my phone if possible. I wasn't able to find an open source app that did this so I just did it myself.

Just wave your hand over the proximity sensor of your phone to turn the screen on. This is called *wave mode* and can be disabled in the settings screen in order to avoid accidental switching on of your screen.

It will also turn on the screen when you take your smartphone out of your pocket or purse. This is called *pocket mode* and can also be disabled in the settings screen.

Both of these modes are enabled by default.

It also locks your phone and turns off the screen if you cover the proximity sensor for one second. This does not have a special name but can nonetheless be changed in the settings screen too. It is also enabled by default.

For those who have never heard proximity sensor before: it is a small thingie that is somewhere near where you put your ear when you speak on the phone. You practically can't see it and it is responsible for telling your phone to switch off the screen when you're on a call.


Known issues
------------

Unfortunately, some smartphones let the CPU on while listening to the proximity sensor. This is called a *wake lock* and causes considerable battery drain. This isn't my fault and I cannot do anything to change this. Other phones will "go to sleep" when the screen is turned off while still listening to the proximity sensor. In this case, the battery drain is practically zero.

On some phones it just doesn't work. I thought this might have to do with the proximity sensor not being recognised but this does not seem to be the case. I am trying to figure out what the problem is but I have a limited number of devices to test the app on and also a limited amount of time.

Uninstalling the app is somewhat cumbersome. You must remove the *device administration* rights that you (probably) allowed when first installing the app. You must go to Settings -> Security -> Device administrators and remove WaveUp as administrator. After this, you should be able to uninstall the app as usual.

Miscellaneous notes
-------------------
At the beginning I was thinking on calling it *Jedi Hand App* because you can do a similar movement to the Jedi mind control trick to turn on your display. In the end I cowardly refrained from it: I read some scary articles - damn you Internet! - about legal issues with *Star Wars* trademarks.

This is the first Android app I have ever written, so beware!

This is also my first small contribution to the open source world. Finally!

It is available in [F-Droid](https://f-droid.org/repository/browse/?fdid=com.jarsilio.android.waveup "WaveUp on F-Droid").
I also plan on uploading it to Google Play if they ever happen to accept my money to create an account.

I would love if you could give me feedback of any kind or contribute in any way!

Thanks for reading!

Open source rocks!!!
